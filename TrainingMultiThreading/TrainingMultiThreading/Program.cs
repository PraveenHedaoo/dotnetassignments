﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TrainingMultiThreading
{
    class Program
    {
        public static Mutex mutex = new Mutex();
        public static Semaphore semaphore = new Semaphore(2, 3);
        public static Thread[] threads = new Thread[10];
        public static Stopwatch mywatch = new Stopwatch();
        static void Main(string[] args)
        {

            //Normal thread execution
            /*
            Thread thread = new Thread(Normal);
            thread.Start();
            //thread.IsBackground = true;  //the thread will exit if main thread exits
            Console.WriteLine("Main Thread Exited");
            */

            //Monitor:Used For Thread Synchronisation(Only one thread will execute the piece of code)
            /*
            Program program = new Program();
            Thread thread = new Thread(program.Monitors);
            Thread thread1 = new Thread(program.Monitors);
            thread.Start();
            thread1.Start();
            */

            //Mutex
            /*
            for (int i = 0; i < 4; i++)
            {
                Thread mycorner = new Thread(Mutexs);
                mycorner.Name = String.Format("Thread{0}", i + 1);
                mycorner.Start();
            }
            */

            //Semaphore:when want to Give Access to more than one thread
            /*
            for(int i = 0; i < 8; i++)
            {
                threads[i] = new Thread(Semaphores);
                threads[i].Name = "thread "+i;
                threads[i].Start();
            }
            */

            //ThreadPool
            /*
            mywatch.Start();
            ThreadPools();          //Method Execution Time using ThreadPool
            mywatch.Stop();
            Console.WriteLine("Time consumed by ProcessWithThreadPoolMethod is : " + mywatch.ElapsedTicks.ToString());          
            mywatch.Reset();
           
            //Method Execution Time without ThreadPool
            mywatch.Start(); 
            ThreadPools1();
            mywatch.Stop();
            Console.WriteLine("Time consumed by ProcessWithoutThreadPoolMethod is : " + mywatch.ElapsedTicks.ToString());
            mywatch.Reset();
            */

            //TPL
            //Parralel.For
            /*
            Parallel.For(0, 10, i =>
            {
                Console.WriteLine("i = {0}, thread = {1}", i,Thread.CurrentThread.ManagedThreadId);
                Thread.Sleep(10);
            });
            */
            Console.Read();
            
        }
        public static void Normal()
        {
            Console.WriteLine("Thread Started");
            Console.ReadLine();
            Console.WriteLine("Thread Exited");
            Console.ReadKey();
        }
        public void Monitors()
        {
            Monitor.Enter(this);
            for(int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }
            Console.ReadKey();
            Monitor.Exit(this);
        }
        public static void Mutexs()
        {
            mutex.WaitOne(); 
            Console.WriteLine("{0} has Entered",Thread.CurrentThread.Name);           
            Thread.Sleep(500);    
            Console.WriteLine("{0} is leaving\n",Thread.CurrentThread.Name);
            mutex.ReleaseMutex();   
        }
        public static void Semaphores()
        {
            semaphore.WaitOne();
            Console.WriteLine("{0} Entered the section",Thread.CurrentThread.Name);
            Thread.Sleep(300);
            Console.WriteLine("{0} Leaving the section",Thread.CurrentThread.Name);
            semaphore.Release();
        }
        public static void ThreadPools()
        {
            for(int i = 0; i < 1000; i++)
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(ThreadPools11));
            }
        }
        public static void ThreadPools1()
        {
            for (int i = 0; i < 1000; i++)
            {
                Thread thread = new Thread(ThreadPools11);
            }
        }
        public static void ThreadPools11(object callback)
        {
        }

    }
}
