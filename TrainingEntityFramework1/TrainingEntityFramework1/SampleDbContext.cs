﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingEntityFramework1
{
    public class SampleDbContext : DbContext
    {
        public SampleDbContext()
        {
            Database.SetInitializer(new SampleIdDBInitializer());
        }
        public DbSet<Departments> Departments { get; set; }
    }

    public class SampleRepository
    {
        SampleDbContext context = new SampleDbContext();

    }
}
