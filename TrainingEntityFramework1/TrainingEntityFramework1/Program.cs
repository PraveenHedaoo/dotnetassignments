﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainingEntityFramework1.Repository;

namespace TrainingEntityFramework1
{
    class Program
    {
        static IRepository<Departments> repository;
        static void Main(string[] args)
        {
            repository = new Repository<Departments>();
            var listofDepartments = GetAll(repository);

            foreach (var item in listofDepartments)
            {
                Console.WriteLine(item.Name);
            }
            Console.ReadKey();
        }

        private static IEnumerable<Departments> GetAll(IRepository<Departments> repository)
        {
            return repository.GetAll();
        }
    }
}
