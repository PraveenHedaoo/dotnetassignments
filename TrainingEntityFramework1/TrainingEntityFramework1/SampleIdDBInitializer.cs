﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TrainingEntityFramework1
{
    public class SampleIdDBInitializer : DropCreateDatabaseAlways<SampleDbContext>
    {
        private List<Departments> departments;
        protected override void Seed(SampleDbContext context)
        {
            
            departments = new List<Departments>
            {
                new Departments(){Name="Name1",Location="Location1"},
                new Departments(){Name = "Name2",Location ="Location2"}
                
            };

            context.Departments.AddRange(departments);
            base.Seed(context);
            
        }
    }
}
