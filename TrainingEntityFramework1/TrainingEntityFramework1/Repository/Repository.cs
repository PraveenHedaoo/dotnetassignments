﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingEntityFramework1.Repository
{
    class Repository<T> : IRepository<T> where T:class 
    {

        private SampleDbContext sampleDbContext;
        private DbSet<T> dbset;

        public Repository()
        {
            sampleDbContext = new SampleDbContext();
            dbset = sampleDbContext.Set<T>();
        }
        public IEnumerable<T> GetAll()
        {
            return dbset.ToList();
        }

        public void save()
        {
            sampleDbContext.SaveChanges();
        }
    }
}
