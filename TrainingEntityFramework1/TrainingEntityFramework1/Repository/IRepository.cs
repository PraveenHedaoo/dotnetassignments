﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingEntityFramework1.Repository
{
    public interface IRepository<T> where T:class
    {
        IEnumerable<T> GetAll();
        void save();
    }
}
