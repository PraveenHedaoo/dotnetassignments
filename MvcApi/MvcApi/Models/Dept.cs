﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApi.Models
{
    public class Dept
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
    }
}