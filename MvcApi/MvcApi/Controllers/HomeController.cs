﻿using MvcApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MvcApi.Controllers
{
    public class HomeController : Controller
    {       
        public ActionResult Index()
        {

            List<Dept> ls= new List<Dept> { };

            using(var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:60297");

                var response = client.GetAsync("/api/Departments");
                response.Wait();

                var result = response.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Dept>>();
                    readTask.Wait();

                    ls = readTask.Result;
                }
            }
            return View(ls);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {

            Dept dept = new Dept();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:60297");

                var response = client.GetAsync("/api/Departments/"+id);
                response.Wait();

                var result = response.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Dept>();                   
                    readTask.Wait();

                    dept = readTask.Result;
                }
            }
            return View(dept);
        }

        [HttpPost]
        public ActionResult Edit(int id,Dept dept)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:57411");

                var putTask = client.PutAsJsonAsync<Dept>("/api/Departments/"+id, dept);
                putTask.Wait();

                var result = putTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Details");
                }
            }
            return View(dept);
        }
    
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(string name,string location,Dept dept)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:60297");

                var postTask = client.PostAsJsonAsync<Dept>("/api/Departments",dept);
                postTask.Wait();

                var result = postTask.Result;


                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Details");
                }
            }
            return View();
        }

        public ActionResult Details(int id)
        {
            Dept dept = new Dept();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:60297");

                var response = client.GetAsync("/api/Departments/" + id);
                response.Wait();

                var result = response.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Dept>();
                    readTask.Wait();

                    dept = readTask.Result;
                }
            }       
            return View();
        }
       
        public ActionResult Delete()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:60297");

                var deleteTask = client.DeleteAsync("/api/Departments");
                //var postTask = client.PostAsJsonAsync<Dept>("/api/Departments");
                deleteTask.Wait();

                var result = deleteTask.Result;


                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Details");
                }
            }
            return View();
        }

       
    }
}