﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_MVC_Api.Models
{
    public class studentviewmodel
    {
        public string name { get; set; }
        public string email { get; set; }
        public Nullable<System.DateTime> join_date { get; set; }
    }
}