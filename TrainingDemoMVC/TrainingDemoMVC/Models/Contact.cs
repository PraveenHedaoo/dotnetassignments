﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TrainingDemoMVC.CustomValidators;

namespace TrainingDemoMVC.Models
{
    public class Contact
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Required]
        [MyCustomValidator(10)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(5)]
        public string LastName { get; set; }
    }
   
}