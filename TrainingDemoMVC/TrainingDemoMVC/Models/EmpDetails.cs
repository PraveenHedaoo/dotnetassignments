﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrainingDemoMVC.Models
{
    public class EmpDetails
    {
        [Required]
        public int Age { get; set; }
        public string Name { get; set; }        
        public string Location { get; set; }
    }
}