﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrainingDemoMVC.Models
{
    public class CustomerAsync
    {

        public string CustName { get; set; }
        public int CustId { get; set; }
        public string CustLocation { get; set; }
        public string ProdName { get; set; }
        public int ProdId { get; set; }
        public int ProdCost { get; set; }
        
    }
}