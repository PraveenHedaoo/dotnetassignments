﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrainingDemoMVC.Models;

namespace TrainingDemoMVC.Controllers
{
    public class EmpController : Controller
    {
        // GET: Emp
       
        public ActionResult Add()
        {
            return View();
        }

        [HttpGet]
        public ActionResult EmpDetails()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EmpDetails(EmpDetails empDetails)
        {           
            string name = empDetails.Name;
            string location = empDetails.Location;
            return Content(name);
        }

       
    }
}