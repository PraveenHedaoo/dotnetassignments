﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TrainingDemoMVC.Models;

namespace TrainingDemoMVC.Controllers
{
    public class AsynchronousController : Controller
    {
        public object Viewbag { get; private set; }

        // GET: Asynchronous
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CustProd()
        {
            
            var watch1 = new Stopwatch();
            watch1.Start();
            List<Customer> listCustProd = new List<Customer> { };           

            for (int i = 0; i < 100; i++)
            {

                var cust = Create(i);
                listCustProd.Add(cust);
                
            }
            watch1.Stop();
            ViewBag.wat1 = "Time Taken: "+ watch1.ElapsedMilliseconds + "ms";
            return View(listCustProd);
        }

        public Customer Create(int i)
        {
            Thread.Sleep(5);
            Customer cust1 = new Customer();
            cust1.CustName = "Praveen " + i;
            cust1.CustLocation = "Pune" + i;
            cust1.CustId = i;
            cust1.ProdId = i + 1;
            cust1.ProdName = "Product" + i;
            cust1.ProdCost = 3000 + i;

            return cust1;
            
        }
       
        public async Task<ActionResult> CustProd1()
        {
           
            var watch = new Stopwatch();
            watch.Start();
            List<CustomerAsync> list1 = new List<CustomerAsync> { };
            for(int i = 0; i < 100; i++)
            {
                var list2 = await Create1(i);
                list1.Add(list2);
            }

            watch.Stop(); 
            
            ViewBag.wat = "Time Taken: " + watch.ElapsedMilliseconds + "ms";
          
            return View(list1);
        } 
        
        public async Task<CustomerAsync> Create1(int i)
        {
            Task.Delay(5);
            CustomerAsync cust1 = new CustomerAsync();

            cust1.CustName = "Praveen " + i;
            cust1.CustLocation = "Pune" + i;
            cust1.CustId = i;
            cust1.ProdId = i + 1;
            cust1.ProdName = "Product" + i;
            cust1.ProdCost = 3000 + i;
                            
            return cust1;
        }
    }
}