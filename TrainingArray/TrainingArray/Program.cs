﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TrainingArray
{
    struct Books
    {
        public string title;
        public string author;
        public string subject;
        public int book_id;

        public void display()
        {
            Console.WriteLine("Title : {0}", title);
            Console.WriteLine("Author : {0}", author);
            Console.WriteLine("Subject : {0}", subject);
            Console.WriteLine("Book_id :{0}", book_id);
        }
    };

    struct Books1
    {
        public int price;

        public void display()
        {
            Console.WriteLine("Price : {0}", price);
        }
    };

    class Program
    {
        enum Days { Sun, Mon, tue, Wed, thu, Fri, Sat };
        public string check { get; set; }
        static void Main(string[] args)
        {

            //Array
            /*
            string name = "praveen";  //String Non-Mutable
            string[] array = new string[name.Length+1];
            for(int i=0;i<name.Length;i++)
            {
                array[name.Length-i-1] = name[i].ToString();
            }
            for (int i = 0; i < name.Length; i++)
            {
                Console.Write(array[i]);
            }
            */

            /*
            Console.WriteLine();
            StringBuilder stringBuilder = new StringBuilder();  //stringBuilder Mutable
            stringBuilder.Append("Hello");
            stringBuilder.Append("World");
            stringBuilder.Append("Good");
            stringBuilder.Append("Morning");
            
            Console.WriteLine(stringBuilder);

            */


            //ArrayList
            /*
            Program p = new Program();
            p.check = "hello";

            ArrayList arrayList = new ArrayList();
            arrayList.Add("Hello");
            arrayList.Add(1);
            arrayList.Add(p.check);
           
            Console.WriteLine(arrayList[2]);
            */


            //Hashtable
            /*
            Hashtable hashtable = new Hashtable();
            hashtable.Add("01", "Praveen");
            hashtable.Add("02", "Hedaoo");
            hashtable.Add("03", 1);

            Console.WriteLine(hashtable.ContainsKey("01"));
            Console.WriteLine(hashtable["01"]);

            ICollection key = hashtable.Keys;  //All the keys in Hashtable are Collected in key
            foreach(string k in key)
            {
                Console.WriteLine(hashtable[k]);
            }
            */
            /*
            List<Generic> ls = new List<Generic> { };
            

            Generic generic = new Generic("Praveen","Hedaoo");
            ls.Add(generic);
            Generic generic1 = new Generic("Praveen1", "Hedaoo1");
            ls.Add(generic1);
            Generic generic2 = new Generic("Praveen2", "Hedaoo2");
            ls.Add(generic2);

            Console.WriteLine(ls[0].str3);

            List<int> lsint = new List<int> { };

            lsint.Add(3);
            lsint.Add(5);
            lsint.Add(4);
            lsint.Add(1);
            lsint.Add(2);

            lsint.Reverse();
            foreach(int i in lsint)
            {
                Console.WriteLine(i);
            }

            Stack<int> stack = new Stack<int> { };
            stack.Push(3);
            stack.Push(5);
            stack.Push(6);
            stack.Push(1);
            Console.WriteLine("Peek "+stack.Peek());
            Console.WriteLine("LIFO "+stack.Pop());

            Queue<string> queue = new Queue<string> { };
            queue.Enqueue("100");
            queue.Enqueue("200");
            queue.Enqueue("300");
            queue.Enqueue("400");
            Console.WriteLine("Peek "+queue.Peek());
            Console.WriteLine("FIFO "+queue.Dequeue());

            */

            //Structure
            /*
            Books Book1;                       
            Book1.title = "C Programming";
            Book1.subject = "C";
            Book1.book_id = 123;
            Book1.author = "Rey";
            Book1.display();

            Books1 books = new Books1();
            books.price = 1234;
            books.display();
            */

            //Tuple
            /*
            var tuple1 = Tuple.Create("Praveen", "Hedaoo", "Pune");
            Console.WriteLine(tuple1.Item1);
           

            Tuple<int,string> tuple2 = new Tuple<int, string>(1, "abc");
           

            var tuple3 = Tuple.Create(1,2,3,"one","two","three","four",Tuple.Create(4,5,6,7));
            Console.WriteLine(tuple3.Item1);
            Console.WriteLine(tuple3.Rest.Item1); //Rest is used to access value higherthan 7 position
            Console.WriteLine(tuple3.Rest.Item1.Item2);
            */

            //Enum


            int WeekdayStart = (int)Days.Mon;
            int WeekdayEnd = (int)Days.Fri;

            Console.WriteLine("Monday: {0}", WeekdayStart);
            Console.WriteLine("Friday: {0}", WeekdayEnd);
            Console.ReadLine();
        }
    }
}
