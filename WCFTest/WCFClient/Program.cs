﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCFClient
{
    class Program
    {
        static void Main(string[] args)
        {

            //Display All Employees
            ServiceReference1.Service1Client client1 = new ServiceReference1.Service1Client();         
            foreach(ServiceReference1.Employee service in client1.GetData())
            {
                Console.WriteLine(service.Id + " " + service.Name);
            }

            //Display Employee for Particular Id
            ServiceReference1.Service1Client client2 = new ServiceReference1.Service1Client();
            string ID;
            Console.WriteLine("Enter Id To Search");
            ID = Console.ReadLine();
            var dis= client2.GetEmployee(ID);
            Console.WriteLine(dis.Id + " " + dis.Name);

            //Add Employee and Display
            string Id;
            string Name;

            Console.WriteLine("Add Employee");           
            Console.WriteLine("Enter Id");
            Id = Console.ReadLine();

            Console.WriteLine("Enter Name");
            Name = Console.ReadLine();           
            ServiceReference1.Service1Client client3 = new ServiceReference1.Service1Client();
            var display = client3.AddEmployee(Id, Name);

            foreach(ServiceReference1.Employee i in display)
            {
                Console.WriteLine(i.Id + " " + i.Name);
            }            
            
            Console.ReadKey();
        }
    }
}
