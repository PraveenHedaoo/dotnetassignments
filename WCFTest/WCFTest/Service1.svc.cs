﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WCFTest
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public string Name;
        public string Id;
        public List<Employee> GetData()
        {
            List<Employee> services = new List<Employee>();

            Employee service1 = new Employee
            {
                Id = "1",
                Name = "Praveen"
            };
            services.Add(service1);

            Employee service2 = new Employee
            {
                Id = "2",
                Name = "Aditya"
            };
            services.Add(service2);

            Employee service3 = new Employee
            {
                Id = "3",
                Name = "Jai"
            };
            services.Add(service3);

            return services;
        }

        public Employee GetEmployee(string Id)
        {
            List<Employee> services = new List<Employee>();

            Employee service1 = new Employee
            {
                Id = "1",
                Name = "Praveen"
            };
            services.Add(service1);

            Employee service2 = new Employee
            {
                Id = "2",
                Name = "Aditya"
            };
            services.Add(service2);

            Employee service3 = new Employee
            {
                Id = "3",
                Name = "Jai"
            };
            services.Add(service3);

            var employee = services.Find(x => x.Id == Id);
            return employee;
        }

        public List<Employee> AddEmployee(string Id,string Name)
        {
            List<Employee> services = new List<Employee>();

            Employee service1 = new Employee
            {
                Id = "1",
                Name = "Praveen"
            };
            services.Add(service1);

            Employee service2 = new Employee
            {
                Id = "2",
                Name = "Aditya"
            };
            services.Add(service2);

            Employee service3 = new Employee
            {
                Id = "3",
                Name = "Jai"
            };
            services.Add(service3);

            Employee service4 = new Employee
            {
                Id = Id,
                Name = Name
            };
            services.Add(service4);

            return services;
        }
    }
}
