﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise
{
    class Program
    {
        public void eg1()
        {
            /*
             1)	Design grading system based on marks scored. Follow below grade range.
                Marks <40 : Fail.
                40<=Marks<50: 3rd class.
                50<=marks<60: 2nd class.
                60<=marks<66: 1st class.
                66<=marks: distinction.
                Accept name and marks as input.
             */
            string name;
            int marks;
            Console.Write("Enter Name\t");
            name = Console.ReadLine();
            Console.Write("Enter Marks\t");
            marks = int.Parse(Console.ReadLine());
            Console.WriteLine(marks);

            if (marks < 40)
            {
                Console.WriteLine(name + " is failed\n");
            }
            else if (marks >= 40 && marks < 50)
            {
                Console.WriteLine(name + " has got 3rd class\n");
            }
            else if (marks >= 50 && marks < 60)
            {
                Console.WriteLine(name + " has got 2nd class\n");
            }
            else if (marks >= 60 && marks < 66)
            {
                Console.WriteLine(name + " has got 1st class\n");
            }
            else if (marks >= 66)
            {
                Console.WriteLine(name + " has got distinction\n");
            }
            else
            {
                Console.WriteLine("Invalid Marks\n");
            }
        }

        public void eg2()
        {
            /*
            2)	Write a program to find the eligibility of admission for a professional course based on the following criteria: 
               Marks in Maths >=65
               Marks in Phy >=55
               Marks in Chem>=50
               Total in all three subject >=180
               or
               Total in Math and Subjects >=140
               Accept the marks as input. Also display the average (do not round off).

    */

            int markm, markp, markc;
            int total, totalms;
            Console.Write("Enter Marks in Maths\t");
            markm = int.Parse(Console.ReadLine());
            Console.Write("Enter Marks in Phy\t");
            markp = int.Parse(Console.ReadLine());
            Console.Write("Enter Marks in Chem\t");
            markc = int.Parse(Console.ReadLine());

            total = (markc + markm + markp) / 3;

            if ((markm >= 65 && markp >= 55 && markc >= 50) && total >= 180)
                Console.WriteLine("You are Eligible,your average is " + total+"\n");
            else
                Console.WriteLine("Yor are not Eligible,Your average is " + total+"\n");


        }

        public void eg3()
        {
            /*
            3)	Write a program to find first 10 multiples of x and y. Find the sum of multiples of x, store it in firstSumOfMultiple.
               Find the sum of multiples of y, store it in secondSumOfMultiple.
               If firstSumOfMultiple is greater than secondSumOfMultiple then perform 
               firstSumOfMultiple /secondSumOfMultiple, else perform secondSumOfMultiple /firstSumOfMultiple.
               Accept x and y as user input, write the output on console.

            */
            int x, y;
            int result;
            Console.WriteLine("Enter X\t");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter Y\t");
            y = int.Parse(Console.ReadLine());

            int firstSumofMultiple = 0, secondSumofMultiple = 0;


            for (int i = 1; i < 11; i++)
            {
                firstSumofMultiple = firstSumofMultiple + (x * i);
                secondSumofMultiple = secondSumofMultiple + (y * i);
            }
            if (firstSumofMultiple > secondSumofMultiple)
            {
                result = firstSumofMultiple / secondSumofMultiple;
                Console.WriteLine($"FirstSum={firstSumofMultiple} no is bigger and division of sum is " + result+"\n");
            }
            else
            {
                result = secondSumofMultiple / firstSumofMultiple;
                Console.WriteLine($"Second={secondSumofMultiple} no is bigger and division of sum is " + result+"\n");
            }

        }

        public void eg4()
        {

            /*
             4)	Write a program to display certain values of the function a = b2 + 2b + 1 (using integer numbers for b , ranging from -5 to +5).
             */

            double a = 0;
            for (int i = -5; i < 6; i++)
            {
                a = Math.Pow(i, 2) + (2 * i) + 1;
                Console.WriteLine("Value is " + a+"\n");
            }
        }
        static void Main(string[] args)
        {
    
            string cont1;
            int ip;
            Program obj = new Program();
            do
            {
                Console.WriteLine("1:Grading System\n2:Eligibility System\n3:Multiples\n4:Function Value\n");
                ip= int.Parse(Console.ReadLine());
                switch (ip)
                {
                    case 1:
                        obj.eg1();
                        break;
                    case 2:
                        obj.eg2();
                        break;
                    case 3:
                        obj.eg3();
                        break;
                    case 4:
                        obj.eg4();
                        break;
                }
                Console.WriteLine("Cont=1;Exit;any");
                cont1 = Console.ReadLine();

            } while (cont1 == "1");

            Console.ReadKey();
        }
    }
}
