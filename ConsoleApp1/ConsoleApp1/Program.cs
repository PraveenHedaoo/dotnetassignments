﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Print Numbers Divisible by 2,3,5
            Console.WriteLine("Numbers Divisible by 2,3,5");
            for (int i = 1; i < 100; i++)
            {
                if(i%2==0  && i%3==0 && i%5==0)
                {
                    Console.WriteLine(i);
                }
            }

            //Prime Numbers from 100-900
            bool a = false;
            for(int i=100;i<901;i++)
            {
                for(int j=2;j<Math.Sqrt(i);j++)
                {
                    if(i%j==0)
                    {
                        a = true;
                        break;
                    }
                    else
                    {
                        a = false;
                    }
                }
                if(a==false)
                {
                    Console.WriteLine(i);
                }
              

            }

            int sum = 0;
            for (int i = 1; i < 21; i++)
            {
                if (i % 3 == 0)
                {
                    sum = sum + i;
                }
            }
            Console.WriteLine(sum);
            Console.Read();
        }
    }
}
