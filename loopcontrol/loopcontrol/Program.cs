﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace loopcontrol
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> colors = new List<string> { "Brown", "Red", "Green" };
            foreach(string color in colors)
            {
                Console.WriteLine(color);
            }
            Console.ReadKey();
        }
    }
}
