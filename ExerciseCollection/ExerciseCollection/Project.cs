﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ExerciseCollection
{
    class Project
    {
        public string lemail { get; set; } 
        public string lpassword { get; set; }
        public string remail { get; set; }
        public string rpassword { get; set; }
        public int Empid { get; set; }
        public string Name { get; set; }
        public int Salary { get; set; }
        public string Position { get; set; }

        public Dictionary<string, string> register = new Dictionary<string, string> { };
        public Dictionary<int, Tuple<string, int, string>> keyValues = new Dictionary<int, Tuple<string, int, string>> { };

        //Display Employee Details
        public void Display()
        {
            foreach (int key in keyValues.Keys)
            {
                Console.WriteLine(key + ":" + keyValues[key]);
            }
        }

        //Register Employee and get Details
        public void Register()
        {
            Console.WriteLine("Registration Form");
            bool isEmail = true;
            do
            {
                Console.WriteLine("Enter Mail-Id to Register");
                remail = Console.ReadLine();
                isEmail = Regex.IsMatch(remail, @"[A-za-z0-9]+[A-za-z0-9#$!\.]*@[A-Za-z]+.com\b",RegexOptions.IgnoreCase);
                if (isEmail == true)
                {
                    Console.WriteLine("correct email-id");
                }
                else
                {
                    Console.WriteLine("Wrong email-id");
                }
            } while (isEmail == false);
            Console.WriteLine("Enter Password to Register");
            rpassword = Console.ReadLine();
            register.Add(remail, rpassword);

            Console.WriteLine("Enter Employee Details\n");
            Console.WriteLine("Enter Employee Id");
            Empid = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter Employee Name");
            Name = Console.ReadLine();
            Console.WriteLine("Enter Employee Salary");
            Salary = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter Employee Position");
            Position = Console.ReadLine();
            var employee = Tuple.Create(Name, Salary, Position);

            keyValues.Add(Empid, employee);
        }

        //Employee Login and Call Display Method to display Employee Details
        public void Login()
        {
            Console.WriteLine("Login Form");
            Console.WriteLine("Enter Mail-Id to Login");
            lemail = Console.ReadLine();
            Console.WriteLine("Enter Password to Login");
            lpassword = Console.ReadLine();
            foreach(string key in register.Keys)
            {
                if(lemail==key && lpassword == register[key])
                {
                    Console.WriteLine("Login Successfully");
                    Display();
                }      
            }
        }


    }
}
