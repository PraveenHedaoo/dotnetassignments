﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace ExerciseCollection
{
    class Program
    {      
        
        public Dictionary<int, Tuple<string,int,string>> dict1 = new Dictionary<int, Tuple<string,int,string>> { };

        public static void showMatch(string text, string expr)
        {
            bool isEmail = Regex.IsMatch(text, expr, RegexOptions.IgnoreCase);
            if (isEmail == true)
            {
                Console.WriteLine("correct email-id");
            }
            else
            {
                Console.WriteLine("Wrong email-id");
            }

        }

        public string email { get; set; }

        public void DisplayEmp()
        {
            foreach(int key in dict1.Keys)
            {
                Console.WriteLine(key+ ":" + dict1[key]);
            }
        }

        static void Main(string[] args)
        {
            Program obj = new Program();
            /*
            //Assign1
            //Take string with email in the string find regex pattern to detect email
            Console.WriteLine("Enter Your Xoriant Mail id");
            obj.email = Console.ReadLine();
            showMatch(obj.email, @"[A-za-z0-9]+[A-za-z0-9#$!\.]*@[A-Za-z]+.com\b");



            //Assign2
            //create dictionary collection of 10 employees ,kae as emp id ,print emp property at even position                           

            int b = 0;
            do
            {
                Console.WriteLine("Add Employee Details");

                Employee emp = new Employee();

                Console.WriteLine("Enter Employee Id");
                emp.Empid = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter Employee Name");
                emp.Name = Console.ReadLine();
                Console.WriteLine("Enter Employee Salary");
                emp.Salary = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter Employee Position");
                emp.Position = Console.ReadLine();
                var employee = Tuple.Create(emp.Name, emp.Salary, emp.Position);

                obj.dict1.Add(emp.Empid,employee);

                Console.WriteLine("1:Add Employee 0:Exit");
                b = int.Parse(Console.ReadLine());
            } while (b == 1);
            obj.DisplayEmp();

            */

            Project project = new Project();
            project.Register();
            project.Login();

            Console.ReadKey();
        }
    }
}
