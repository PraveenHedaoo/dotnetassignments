﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise2
{
    public class Class2
    {
        public delegate int NumberChanger(int n);
        public event NumberChanger Num;

        public void Add(int n)
        {
            int num = 100;
            num += n;
            Num(num);
        }
        public void Sub(int n)
        {
            int num = 100;
            num -= n;
            Num(num);
        }
        public void Mul(int n)
        {
            int num = 100;
            num *= n;
            Num(num);
        }

    }
}
