﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exercise1;
using Exercise2;

namespace ExerciseDelegates
{

    delegate void CustomDel(string s);
    class Program
    {
        static void Good(string s)
        {
            System.Console.WriteLine("  Good, {0}!", s);
        }

        static void Morning(string s)
        {
            System.Console.WriteLine("  Morning, {0}!", s);
        }

        public void FToC()
        {
            int fahrenheit;
            Double celsius,fahrenheit1;
            Console.WriteLine("Enter Fahrenheit to Convert To Celsius");       
            fahrenheit = int.Parse(Console.ReadLine());
            fahrenheit1 = Convert.ToDouble(fahrenheit);
            celsius = (fahrenheit1 - 32) * 5 / 9;
            Console.WriteLine("Celsius: " + celsius);

        }

        public void DtoW()
        {
            string number;
            Console.WriteLine("Enter Number To Convert To Words");
            number = Console.ReadLine();
            string[] digits = { "zero", "one", "two","three", "four", "five",  "six", "seven", "eight", "nine" };
            string result = "";

            foreach(int i in number)
            {
                if (i == '0')
                {
                    result = result + digits[0] + " ";
                    continue;
                }
                else if (i == '1')
                {
                    result = result + digits[1] + " ";
                    continue;
                }
                else if (i == '2')
                {
                    result = result + digits[2] + " ";
                    continue;
                }
                else if (i == '3')
                {
                    result = result + digits[3] + " ";
                    continue;
                }
                else if (i == '4')
                {
                    result = result + digits[4] + " ";
                    continue;
                }
                else if (i == '5')
                {
                    result = result + digits[5] + " ";
                    continue;
                }
                else if (i == '6')
                {
                    result = result + digits[6] + " ";
                    continue;
                }
                else if (i == '7')
                {
                    result = result + digits[7] + " ";
                    continue;
                }
                else if (i == '8')
                {
                    result = result + digits[8] + " ";
                    continue;
                }
                else if (i == '9')
                {
                    result = result + digits[9] + " ";
                    continue;
                }
            }
         
            Console.WriteLine(result);

        }
        public static double DisplayInches(double inches)
        {
            Console.WriteLine("Feet-To-Inches: " + inches);
            return inches;
        }
        public static int DisplayAdd(int a)
        {
            Console.WriteLine("Addition: " + a);
            return a;
        }
        public static int DisplaySub(int a)
        {
            Console.WriteLine("Subtraction: " + a);
            return a;
        }
        public static int DisplayMul(int a)
        {
            Console.WriteLine("Multiplication: " + a);
            return a;

        }
        public string ConvertNumbertoWords(long number)
        {
            if (number == 0) return "ZERO";
            
            string words = "";          
            if ((number / 1000) > 0)
            {
                words += ConvertNumbertoWords(number / 1000) + " THOUSAND ";
                number %= 1000;
            }
            if ((number / 100) > 0)
            {
                words += ConvertNumbertoWords(number / 100) + " HUNDRED ";
                number %= 100;
            }
             
            if (number > 0)
            {
                if (words != "") words += "AND ";
                var unitsMap = new[]
                {
            "ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN"
        };
                var tensMap = new[]
                {
            "ZERO", "TEN", "TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY"
        };
                if (number < 20) words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0) words += " " + unitsMap[number % 10];
                }
            }
            return words;
        }
        public static void Main(string[] args)
        {
            //(Exercise 1)
            /*
            double feet;
            Class1 obj = new Class1();
            obj.wheretosend += DisplayInches;
            Console.WriteLine("Enter feets to calculate inches");
            feet = Double.Parse(Console.ReadLine());
            obj.Calculate(feet);
            */

            //(Exercise 2)
            /*
            Class2 obj = new Class2();

            int no;
            Console.WriteLine("Enter number to perform arithmetic operations");
            no = int.Parse(Console.ReadLine());

            obj.Num += DisplayAdd;
            obj.Add(no);
            obj.Num -= DisplayAdd;

            obj.Num += DisplaySub;
            obj.Sub(no);
            obj.Num -= DisplaySub;

            obj.Num += DisplayMul;
            obj.Mul(no);
            obj.Num -= DisplayMul;
            */

            //Exercise 3
            /*
            CustomDel hiDel, byeDel, multiDel;
            hiDel = Good;
            byeDel = Morning;
            multiDel = hiDel + byeDel;

            Console.WriteLine("Invoking delegate GoodDel:");
            hiDel("A");
            Console.WriteLine("Invoking delegate MorningDel:");
            byeDel("B");
            Console.WriteLine("Invoking delegate multiDel:");
            multiDel("C");
            */



            //Fahrenheit To Celsius (Exercise 4)
            //Program.FToC();

            //Digits To Words    (Exercise 5)
            //Program program = new Program();
            //program.DtoW();
            
            /*
            Program program = new Program();
            string result;
            result=program.ConvertNumbertoWords(1234);
            Console.WriteLine(result);
            */

            Console.ReadKey();

        }
         
    }
}
