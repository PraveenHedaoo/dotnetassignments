﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practise
{
    class Program:Customer
    {
        public int cust { get; set; }
        public int custD { get; set; }
        public string Prodname { get; set; }
        public string Desc { get; set; }
        public int Curprice { get; set; }
        public string Prodreq { get; set; }
        public int QuaReq { get; set; }
        public void input(int cust)
        {
            Console.WriteLine("Enter Firstname");
            Firstname = Console.ReadLine();
            Console.WriteLine("Enter Lastname");
            Lastname = Console.ReadLine();
            Console.WriteLine("Enter Email");
            Email = Console.ReadLine();
            Console.WriteLine("Enter HomeAdd");
            HomeAdd = Console.ReadLine();
            Console.WriteLine("Enter WorkAdd");
            WorkAdd = Console.ReadLine();

            if (cust == 1)
            {
                Customer Bus = new Customer(Firstname, Lastname, Email, HomeAdd, WorkAdd);
                Bussines.Add(Bus);

                
            }
            else if (cust == 2)
            {
                Customer Res = new Customer(Firstname, Lastname, Email, HomeAdd, WorkAdd);
                Residential.Add(Res);
            }
            else if (cust == 3)
            {
                Customer Gov = new Customer(Firstname, Lastname, Email, HomeAdd, WorkAdd);
                Goverment.Add(Gov);
            }
        }
        public void Display(int custD)
        {
            if (custD == 1)
            {

                Console.WriteLine("Firstname: " + Bussines[0].Firstname + "Lastname: " + Bussines[0].Lastname +
                    "Email: " + Bussines[0].Email + "HomeAdd: " + Bussines[0].HomeAdd + "WorkAdd: " + Bussines[0].WorkAdd);
            }
            else if (custD == 2)
            {
                Console.WriteLine("Firstname: " + Residential[0].Firstname + "Lastname: " + Residential[0].Lastname +
                   "Email: " + Residential[0].Email + "HomeAdd: " + Residential[0].HomeAdd + "WorkAdd: " + Residential[0].WorkAdd);
            }
            else if (custD == 3)
            {
                Console.WriteLine("Firstname: " + Goverment[0].Firstname + "Lastname: " + Goverment[0].Lastname +
                      "Email: " + Goverment[0].Email + "HomeAdd: " + Goverment[0].HomeAdd + "WorkAdd: " + Goverment[0].WorkAdd);
            }
        }

        public void AddProduct()
        {
            Console.WriteLine("Enter Product Name");
            Prodname = Console.ReadLine();
            Console.WriteLine("Enter Product Description");
            Desc = Console.ReadLine();
            Console.WriteLine("Enter Product Price");
            Curprice = int.Parse(Console.ReadLine());

            Products product = new Products(Prodname,Desc,Curprice);
            CurProd.Add(product);
            
        }

        public void DisplayProd()
        {
            
            Console.WriteLine("ProdName: " + CurProd[0].Prodname + "Prod Desc: " + CurProd[0].Desc +
                "Prod Price: " + CurProd[0].Curprice);
        }
        public void getOrder()
        {
            Console.WriteLine("Enter Product Name");
            Prodreq = Console.ReadLine();
            for(int i = 0; i < CurProd.Count; i++)
            {
                if (CurProd[i].Prodname == Prodreq)
                {
                    Console.WriteLine("Product Available");
                    Console.WriteLine("ProdName: " + CurProd[0].Prodname + "Prod Desc: " + CurProd[0].Desc +
                "Prod Price: " + CurProd[0].Curprice);
                    Console.WriteLine("Enter Quantity Required");
                    QuaReq = int.Parse(Console.ReadLine());
                    Console.WriteLine("Order Received");
                    Console.WriteLine("Invoice");
                    Console.WriteLine("ProdName: " + CurProd[0].Prodname + "Prod Desc: " + CurProd[0].Desc +
                "Prod Price: " + CurProd[0].Curprice+"Total Price: "+(QuaReq*CurProd[i].Curprice));

                }
                else
                {
                    Console.WriteLine("Product not Available");
                }
            }
        }

        static void Main(string[] args)
        {
            
            Program customer = new Program();
            Console.WriteLine("Which Customer to Add?");
            Console.WriteLine("1:Business 2:Residential 3:Goverment");
            customer.cust = int.Parse(Console.ReadLine());
            customer.input(customer.cust);
            
            Console.WriteLine("Which Customer to Display?");
            Console.WriteLine("1:Business 2:Residential 3:Goverment");
            customer.custD = int.Parse(Console.ReadLine());
            customer.Display(customer.custD);
            
            Program addproduct = new Program();
            addproduct.AddProduct();
            addproduct.DisplayProd();
            addproduct.getOrder();

            
            
            Console.ReadKey();
        }
    }
}
