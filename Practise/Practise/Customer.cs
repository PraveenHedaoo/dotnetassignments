﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practise
{
    class Customer:Products
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string HomeAdd { get; set; }
        public string Email { get; set; }
        public string WorkAdd { get; set; }
        public List<Customer> Bussines = new List<Customer>();
        public List<Customer> Residential = new List<Customer>();
        public List<Customer> Goverment = new List<Customer>();

        public Customer()
        {

        }
        public Customer(string firstname,string lastname,string email,string homeAdd,string workAdd)
        {
            Firstname = firstname;
            Lastname = lastname;
            Email = email;
            HomeAdd = homeAdd;
            WorkAdd = workAdd;
        }
    
    }
}
