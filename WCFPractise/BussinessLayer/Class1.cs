﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer
{
    public class CustomerOperation
    {
        List<student> data = new List<student>();

        public List<student> AddStudent(int ID, string Name, string Email)
        {
            StudentEntities entities = new StudentEntities();
            student cust = new student();
            cust.id = ID;
            cust.name = Name;
            cust.email = Email;
            entities.students.Add(cust);
            entities.SaveChanges();

            var productEntity = (from p in entities.students select p).ToList();
            return productEntity;
        }

        public string Delete(int ID)
        {
            StudentEntities entities = new StudentEntities();
            student cust = new student
            {
                id = ID
            };
            entities.students.Attach(cust);
            entities.students.Remove(cust);
            entities.SaveChanges();
            return "OK";
        }

        public student DisplayById1(int ID)
        {
            StudentEntities entities1 = new StudentEntities();
            var productEntity = (from p in entities1.students select p).ToList();
            var dis = productEntity.Find(x => x.id == ID);
            return dis;
        }
    }
}
