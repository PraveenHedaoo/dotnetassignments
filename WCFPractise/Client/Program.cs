﻿using Client.ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> date { get; set; }

        static void Main(string[] args)
        {

            Program program = new Program();
            StudentClient client = new StudentClient();
            
            //display using [MessageContract]
            var dis=client.DetailsMessage("pass");
            foreach (student stud in dis)
            {
                Console.WriteLine("ID:" + stud.id + "  Name:" + stud.name + "   Email:" + stud.email + "    Joining:" + stud.join_date);
            }

            //Display all students
            var details = client.Details();
            foreach (student stud in details)
            {
                Console.WriteLine("ID:" + stud.id + "  Name:" + stud.name + "   Email:" + stud.email + "    Joining:" + stud.join_date);
            }

           // Add students and Display
            Console.WriteLine("Add Student");

            Console.WriteLine("Enter ID");
            program.Id = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter Name");
            program.Name = Console.ReadLine();
            Console.WriteLine("Enter Email");
            program.Email = Console.ReadLine();
            StudentClient client1 = new StudentClient();

            var add = client1.AddStudent(program.Id, program.Name, program.Email);
            foreach (student stud in add)
            {
                Console.WriteLine("ID:" + stud.id + "  Name:" + stud.name + "   Email:" + stud.email + "    Joining:" + stud.join_date);
            }

            //Search Student by Id 
            StudentClient client2 = new StudentClient();
            try
            {
                Console.WriteLine("Enter Id to Search");
                program.Id = int.Parse(Console.ReadLine());
                var emp = client2.DisplayById(program.Id);
                Console.WriteLine("Name:" + emp.name + "Email:" + emp.email + "Date:" + emp.join_date);
            }
            catch (FaultException ex)
            {
                Console.WriteLine(ex + "Error Praveen");
            }
            Console.ReadKey();
        }
    }
}
