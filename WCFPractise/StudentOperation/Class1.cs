﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCFPractise;

namespace StudentOperation
{
    public class CustomerOperation : IStudent
    {
        List<student> data = new List<student>();

        List<WCFPractise.student> IStudent.AddStudent(int ID, string Name, string Email)
        {
            StudentEntities entities = new StudentEntities();
            student cust = new student();
            cust.id = ID;
            cust.name = Name;
            cust.email = Email;
            entities.students.Add(cust);
            entities.SaveChanges();

            var productEntity = (from p in entities.students select p).ToList();
            return productEntity;
        }

        WCFPractise.student IStudent.DisplayById(int ID)
        {
            StudentEntities entities1 = new StudentEntities();
            var productEntity = (from p in entities1.students select p).ToList();
            var dis = productEntity.Find(x => x.id == ID);
            return dis;
        }
    }
}
