﻿using BussinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WCFPractise
{

    [ServiceContract]
    public interface IStudent
    {
        [OperationContract(IsOneWay =true)]
        void AddStudent(int ID,string Name,string Email);

        [OperationContract]
        string UpdateStudent(int ID, string Name, string Email);

        [OperationContract]
        student DisplayById(int ID);

        [OperationContract]
        string Delete(int ID);

        [OperationContract]
        List<student> Display();
    }


    [DataContract]
    public class Cust
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public Nullable<System.DateTime> join_date { get; set; }
    }

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class CustomerOperations : IStudent
    {
        List<student> data = new List<student>();

        public void AddStudent(int ID, string Name, string Email)
        {
            StudentEntities entities = new StudentEntities();
            student cust = new student();
            cust.id = ID;
            cust.name = Name;
            cust.email = Email;
            entities.students.Add(cust);
            entities.SaveChanges();

            //var productEntity = (from p in entities.students select p).ToList();
            //return productEntity;
            return;
        }

        public string Delete(int ID)
        {
            StudentEntities entities = new StudentEntities();

            student cust = new student
            {
                id = ID
            };
            entities.students.Attach(cust);
            entities.students.Remove(cust);           
            entities.SaveChanges();           
            return "OK";           
        }

        public List<student> Display()
        {
            StudentEntities entities1 = new StudentEntities();
            // var productEntity = (from p in entities1.students select p).ToList();
            var productEntity = entities1.students.ToList();
            return productEntity;
            
        }

        public student DisplayById(int ID)
        {
            StudentEntities entities1 = new StudentEntities();
            var productEntity = (from p in entities1.students select p).ToList();
            var dis = productEntity.Find(x => x.id == ID);
            return dis;

        }

        public string UpdateStudent(int ID, string Name, string Email)
        {
            StudentEntities entities = new StudentEntities();
            var result=entities.students.Single(change => change.id == ID);
            if (result != null)
            {
                result.id = ID;
                result.name = Name;
                result.email = Email;
                entities.SaveChanges();
            }
            return "Student Updated";
        }
    }
}
