﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading;

namespace WcfOddEven
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall,ConcurrencyMode = ConcurrencyMode.Single)]
    public class Service1 : IService1
    {

        //---------------------------------------------------------------------------------------------------
        //public string GetData(int value)
        //{
        //    if (value % 2 == 0)
        //    {
        //        return string.Format("You entered: {0} is a even value", value);
        //    }
        //    else
        //    {
        //        return string.Format("You entered: {0} is a odd value", value);
        //    }

        //}

        //public string GetRequestReply()
        //{
        //    DateTime dateTime = DateTime.Now;
        //    Thread.Sleep(5000);
        //    DateTime dateTime1 = DateTime.Now;
        //    return ("Now: "+dateTime+"\nAfter:"+dateTime1);
        //}

        //public void OneWay()
        //{
        //    Thread.Sleep(5000);
        //    return;
        //}
        //-----------------------------------------------------------------------------------------------------
        //int count = 0;

        //public int Increment()
        //{
        //    count++;
        //    return count;
        //}

        //-------------------------------------------------------------------------------------------------------------
        public int i;
        public string Call(string Name)
        {
            i++;
            Thread.Sleep(5000);
            return("ClientName:" + Name + "  Instance:" + i.ToString() + "  Thread:" + Thread.CurrentThread.ManagedThreadId + "  Time:" + DateTime.Now);

        }

        //----------------------------------------------------------------------------------------------------

    }
}
