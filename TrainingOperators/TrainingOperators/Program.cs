﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Factorial;

namespace TrainingOperators
{
    class Program
    {
        
        static void Main(string[] args)
        {
            /*
            SimpleClass simple = new SimpleClass();
            simple.iNum = 10;
            simple.MyFunction();

            Customer[] customer = new Customer[10];
            int? length = customer?.Length;
            Console.WriteLine("Customers Array Length " + length);
            Customer first = customer?[0];
            Console.WriteLine("First Customer in array: " + first?.Name);
            int? count = customer?[0]?.Orders?.Count();
            
    
            //Postfix increment

            int num1 = 10;
            Console.WriteLine("Value of Num1 Before Execution " + num1++);
            Console.WriteLine("Value of Num1 After Execution " + num1);

            //Prefix increment
            int num2 = 10;
            Console.WriteLine("\nValue of Num2 Before Execution " + ++num2);
            Console.WriteLine("Value of Num2 After Execution " + num2);
            

            int num1 = 10;
            //TypeOf operator

            Console.WriteLine("Type of Num1 is " + typeof(int).ToString());
            Console.WriteLine("Type of Customer is " + typeof(Customer).ToString());

            //SizeOf operator

            Console.WriteLine("Size of num1 is " + sizeof(int).ToString());

            //Is operator

            Customer cust = new Customer();           
            Console.WriteLine("Check Customer Object " + (cust is Customer));
            Console.WriteLine("Check Customer Object "+(num1 is Customer));
            */

            //Delegates
            Factorial1 fc = new Factorial1();
            //subscribe
            fc.wheretosend += DisplayFactorial;
            fc.wheretosend += DisplayFactorialWhenGreaterThan100;
            fc.wheretosend += number => { Console.WriteLine("Lambda " + number); };
            fc.Calculate(5);
            //unsubscribe
            fc.wheretosend -= DisplayFactorial;
            fc.wheretosend -= DisplayFactorialWhenGreaterThan100;


            Console.ReadLine();
        }
         static void DisplayFactorial(int result)
        {
            Console.WriteLine("CallBack "+result);
        }
        static void DisplayFactorialWhenGreaterThan100(int result)
        {
            if (result > 100)
            {
                Console.WriteLine("Big Value");
            }
            else
            {
                Console.WriteLine("Small Value");
            }
        }
    }
}
