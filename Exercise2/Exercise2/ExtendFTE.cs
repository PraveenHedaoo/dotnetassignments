﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise2
{
    public static class ExtendFTE
    {
        
        public static void Expe(this FullTimeEmployee emp)
        {
            if (emp.Exp < 5)
            {
                Console.WriteLine("Experience of " + emp.Name + " is less than 5 years");
            }
            else if (emp.Exp == 5)
            {
                Console.WriteLine("Experience of " + emp.Name + " is equal to 5 years");
            }
            else
            {
                Console.WriteLine("Experience of " + emp.Name + " is more than 5 years");
            }
        }
        
    }
}
