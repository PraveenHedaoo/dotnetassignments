﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise2
{
    public static class ExxtendContractor
    {
        public static void Expc(this Contractor con)
        {
            if (con.Exp < 5)
            {
                Console.WriteLine("Experience of " + con.Name + " is less than 5 years");
            }
            else if (con.Exp == 5)
            {
                Console.WriteLine("Experience of " + con.Name + " is equal to 5 years");
            }
            else
            {
                Console.WriteLine("Experience of " + con.Name + " is more than 5 years");
            }
        }
    }
}
