﻿using System;
using System.Collections.Generic;

namespace Exercise2
{
    public class Person : IPerson
    {


        public string Name { get; set; }
        public string Desg { get; set; }
        public int Age { get; set; }
        public int Rate { get; set; }
        public int Salary { get; set; }
        public int Id { get; set; }
        public string Address { get; set; }
        public int Exp { get; set; }
        public static bool Agec { get; set; }
        public int No { get; set; }
        public List<FullTimeEmployee> EmpObj = new List<FullTimeEmployee>();
        public List<Contractor> ConObj = new List<Contractor>();

        public void Empinput()
        {

            Console.WriteLine("Enter Name");
            Name = Console.ReadLine();

            Console.WriteLine("Enter Designation");
            Desg = Console.ReadLine();

            Console.WriteLine("Enter Age");
            Age = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter Salary");
            Salary = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter Id");
            Id = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter Address");
            Address = Console.ReadLine();

            Console.WriteLine("Enter Experience\t");
            Exp = int.Parse(Console.ReadLine());

            var emp = new FullTimeEmployee(Name, Desg, Age, Salary, Id, Address, Exp);

            EmpObj.Add(emp);
        }

        public void EmpDisplay()
        {
            Console.WriteLine("Enter Employee No For Details");
            No = int.Parse(Console.ReadLine());
            EmpObj[No].Display();
        }
        public void Compare()
        {
            var em = new FullTimeEmployee();
            em.CompareDesg(EmpObj[0], EmpObj[1]);

        }
        public void EmpOverloading()
        {
            Agec = EmpObj[0] > EmpObj[1];                        //Operator Overloading For comparing Age

            if (Agec == true)
            {
                Console.WriteLine(EmpObj[0].Name + "is Older than " + EmpObj[1].Name);
            }
            else
            {
                Console.WriteLine(EmpObj[0].Name + " is Younger than " + EmpObj[1].Name);
            }

        }
        public void EmpExperience()
        {
            EmpObj[0].Expe();

        }




        public void Coninput()
        {

            Console.WriteLine("Enter Name");
            Name = Console.ReadLine();

            Console.WriteLine("Enter Age");
            Age = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter Rate");
            Rate = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter Experience\t");
            Exp = int.Parse(Console.ReadLine());

            Contractor con = new Contractor(Name, Age, Rate, Exp);
            ConObj.Add(con);

        }

        public void ConDisplay()
        {
            Console.WriteLine("Enter Contractor No For Details");
            No = int.Parse(Console.ReadLine());
            ConObj[No].Display();
        }

        public void ConAge()
        {
            var con = new Contractor();
            con.CompareAge(ConObj[0], ConObj[1]);


        }
        public void ConExperience()
        {
            ConObj[0].Expc();

        }

        static void Main(string[] args)
        {

            

            var person = new Person();
            person.Empinput();
            person.Empinput();
            person.EmpDisplay();
            person.Compare();
            person.EmpExperience();
            

            var person1 = new Person();
            person1.Coninput();
            person1.Coninput();
            person1.ConDisplay();
            person1.ConAge();
            person1.ConExperience();
            


            Console.ReadKey();

        }
    }
}
