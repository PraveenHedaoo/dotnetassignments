﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise2
{
    public class FullTimeEmployee:Person
    {

        public FullTimeEmployee()
        {

        }
        public void Display()
        {
            Console.WriteLine("\nName: " + Name);
            Console.WriteLine("Desg: " + Desg);
            Console.WriteLine("Age: " + Age);
            Console.WriteLine("Salary: " + Salary);
            Console.WriteLine("Id: " + Id);
            Console.WriteLine("Address: " + Address);
            Console.WriteLine("Experience: " + Exp);
        }

        public void CompareDesg(FullTimeEmployee emp1, FullTimeEmployee emp2)
        {
            if (emp1.Desg == emp2.Desg)
            {
                Console.WriteLine("\n"+emp1.Name + " has same Designation as " + emp2.Name + " Designation is " + emp1.Desg);
            }
            else
            {
                Console.WriteLine("\n"+emp1.Name + " has Different Designation than " + emp2.Name);
            }

        }

        public static bool operator> (FullTimeEmployee emp1, FullTimeEmployee emp2)
        {
            if (emp1.Age > emp2.Age)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public static bool operator <(FullTimeEmployee emp1, FullTimeEmployee emp2)
        {
            if (emp1.Age < emp2.Age)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public FullTimeEmployee(string name,string desg,int age,int salary,int id,string address,int exp)
        {
            Name = name;
            Desg = desg;
            Age = age;
            Salary = salary;
            Id = id;
            Address = address;
            Exp = exp;
        }


    }
}
