﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise2
{
    public class Contractor:Person
    {

        public Contractor()
        {

        }
        public void Display()
        {
            Console.WriteLine("\nName: " + Name);
            Console.WriteLine("Rate: " + Rate);
            Console.WriteLine("Age: " + Age);
            Console.WriteLine("Experience: " + Exp);
        }

    

        public void CompareAge(Contractor emp1, Contractor emp2)
        {
            if (emp1.Age > emp2.Age)
            {
                Console.WriteLine("\n" + emp1.Name + " is Older than " + emp2.Name);
            }
            else if (emp1.Age < emp2.Age)
            {
                Console.WriteLine("\n" + emp1.Name + " is Younger than " + emp2.Name);
            }
            else
            {
                Console.WriteLine("\n" + emp1.Name + " has Same Age as " + emp2.Name);
            }
        }

        public Contractor(string name,int age,int rate,int exp)
        {
            Name = name;
            Age = age;
            Rate = rate;
            Exp = exp;
        }
    }
}
