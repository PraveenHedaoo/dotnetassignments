﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingLinq
{
    class Program
    {
        public static string[] musicalArtists = { "Adele", "Maroon 5", "Avril Lavigne" };
        static void Main(string[] args)
        {
            List<string> aArtists =
                (from artist in musicalArtists
                where artist.StartsWith("A")
                select artist).ToList<string>();

            foreach (var artist in aArtists)
            {
                Console.WriteLine(artist);
            }

            Console.ReadKey();
        }
    }
}
